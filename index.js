'use strict';

var express = require('express');
var app = express();
var async = require('async');
var fs = require('fs');
var nunjucks = require('nunjucks');
var env = nunjucks.configure('views', {
    autoescape: true,
    express: app,
    watch: true
});

// TODO: Make this a separate NPM module, or make an NPM Nunjucks filter module

env.addFilter('limitTo', function(input, limit) {
	'use strict';
	if(typeof limit !== 'number'){
		return input;
	}
	if(typeof input === 'string'){
		if(limit >= 0){
			return input.substring(0, limit);
		} else {
			return input.substr(limit);
		}
	}
	if(Array.isArray(input)){
		limit = Math.min(limit, input.length);
		if(limit >= 0){
			return input.splice(0, limit);
		} else {
			return input.splice(input.length + limit, input.length);
		}
	}
	return input;
});

app.get('/', function(req, res) {
	
        async.waterfall([
        function cfInit(asyncCallback) {
            
            var data = {};
            
            return asyncCallback(null, data);
            
        },
        function cfGetContent(data, asyncCallback){
            
            fs.readFile('json/content.json', 'utf8', function (err, fileContents) {
                if (err){return asyncCallback(err)};
                data.content = JSON.parse(fileContents);
                return asyncCallback(null, data);
            });
            
        },
        function cfGetItems(data, asyncCallback) {
            
            fs.readFile('json/slides.json', 'utf8', function (err, fileContents) {
                if (err){return asyncCallback(err)};
                data.slides = JSON.parse(fileContents);
                return asyncCallback(null, data);
            });
            
        }
        ],
        function cfResponse(err, data) {
            
            return res.render('./index.html', {
                content : data.content,
                slides : data.slides
            });
            
        });
        
});

// Static assets
app.use(express.static('public/dist'));

// Ports
app.listen(3000);

console.log('Listening on port 3000');