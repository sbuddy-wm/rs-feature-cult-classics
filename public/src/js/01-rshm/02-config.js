/**
 * @fileOverview Namespace configuration
 * @author Juan David Andrade <juan.andrade@zemoga.com>
 * @version 1.0.0
 */
window.rshm = {
	name: '20 Most Beloved TV Characters',
	version: '1.0.0',
	components: {},
	utils: {},
	currentSection: null
};