/**
 * @fileOverview Global utilities
 * @author Juan David Andrade <juan.andrade@zemoga.com>
 * @version 1.0.0
 */

/*global jQuery, $ */
(function(rshm, $) {

  /**
   * Define input events based on touch support
   */
  rshm.Input = (function() {
    return ('ontouchstart' in window) ? {
      START: 'touchstart',
      MOVE: 'touchmove',
      END: 'touchend',
      CLICK: 'touchstart',
      RESIZE: 'orientationchange',
      touch: true
    } : {
      START: 'mousedown',
      MOVE: 'mousemove',
      END: 'mouseup',
      CLICK: 'click',
      RESIZE: 'resize',
      touch: false
    };
  })();

  /**
   * Global helpers
   */
  rshm.utils = {
    /**
     * Get URL param
     * @param  {String} item - Requested variable
     * @return {*} Variable value, if found
     */
    getQueryVariable: function (item) {
      var svalue = location.search.match(new RegExp('[?&]' + item + '=([^&]*)(&?)','i'));
      return svalue ? svalue[1] : svalue;
    }
  };


  /**
   * Adds css prefixes depending on user's browser
   * @return {Object} CSS prefixed properties
   */
  window.Prefixr = (function() {
    var _cssProperties = {
      textShadow: "textShadow",
      borderRadius: "borderRadius",
      transform: "transform",
      transitionDuration: "transitionDuration",
      boxShadow: "boxShadow",
      transition: "transition",
      transitionDelay: "transitionDelay"
    };

    var _vendorsArray = ['', 'webkit', 'Webkit', 'moz', 'Moz', 'o', 'ms', 'Ms'],
        _eventsArray = {
          'WebkitTransition': 'webkitTransitionEnd',
          'MozTransition': 'transitionend',
          'transition': 'transitionend'
        };

    (function() {
      var i,
        tempProp,
        vendorsLength = _vendorsArray.length;

      //  looping into css properties object  
      for (var prop in _cssProperties) {
        //  looping into vendor types
        for (i = 0; i <= vendorsLength; ++i) {
          _cssProperties[prop] = null;
          tempProp = prop;
          //  capitalize CSS property
          if (_vendorsArray[i] !== '') {
            tempProp = prop.replace(/(^[a-z]{0,1})([\w])/g, replaceKey);
          }

          //  property found
          if (typeof document.documentElement.style[_vendorsArray[i] + tempProp] !== 'undefined') {
            _cssProperties[prop] = _vendorsArray[i] + tempProp;
            break;
          }
        }
      }

      _cssProperties.transitionend = _eventsArray[_cssProperties.transition];

    }());

    function replaceKey(m, key, value) {
      return key.toString().toUpperCase() + value;
    }

    return _cssProperties;
  }());

  //  handle events invoking directly a method inside the DOM Element
  if (!Element.prototype.addEventListener) {
    Element.prototype.addEventListener = function(type, handler, useCapture) {
      if (this.attachEvent) {
        this.attachEvent('on' + type, function(event) {
          event.preventDefault = function() {
            event.returnValue = false;
            return false;
          };

          event.stopPropagation = function() {
            window.event.cancelBubble = true;
            return false;
          };

          event.target = event.srcElement;
          event.currentTarget = event.srcElement;


          handler(event);
        });
      }
      return this;
    };
  }

  if (!Element.prototype.removeEventListener) {
    Element.prototype.removeEventListener = function(type, handler, useCapture) {
      if (this.detachEvent) {
        this.detachEvent('on' + type, handler);
      }
      return this;
    };
  }

})(window.rshm || {}, jQuery || $);