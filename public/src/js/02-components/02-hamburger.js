/**
 * @fileOverview Main navigation for mobile devices
 * @author Juan David Andrade <juan.andrade@zemoga.com>
 */

/* global Utils:false */
/*global jQuery */
/*global Modernizr */
/*global $ */

(function (rshm, $) {
	'use strict';

	/**
	 * Represents a Mobile Menu
	 * @constructor
	 * @return {Object} Exposed methods
	 */
	rshm.Hamburger = function(selector, options) {

		/**
		 * Main container
		 * @type {HTMLElement}
		 */
		var element,
		/**
		 * Document Body
		 * @type {HTMLElement}
		 */
			body,
		/**
		 * Button Icon
		 * @type {HTMLAnchorElement}
		 */
			button,

			menu,

			menuArea,

			closeBtn,

			isOpen = false,
		/**
		 * Default settings
		 * @type {Object}
		 */
			SETTINGS = {
				body: '.main-content',
				button: '.navbar-icon',
				menu: '.main-navbar',
				menuArea: '.menu-backdrop',
				open: 'menu-open',
				close: '.close-btn'
			},
			DEVICE_BREAKPOINT = 1024;

		/**
		 * @construcs NS.Template
		 */
		(function () {
			SETTINGS = $.extend(SETTINGS, options);

			element = (typeof selector === 'string') ? document.querySelector(selector) : selector;
			body = document.querySelector(SETTINGS.body);
			button = element.querySelector(SETTINGS.button);
			menu = document.querySelector(SETTINGS.menu);
			closeBtn = menu.querySelector(SETTINGS.close);
			menuArea = document.querySelector(SETTINGS.menuArea);

			init();

			if (!Prefixr.transform) {
				hideMenu();
			}
		}());

		function init() {
			resize();
			// show navigation
			setTimeout(function() {
				$(menu).show();
			}, 600);
			addEventListeners();
		}

		function resize() {

			if (!Prefixr.transform) {
				return;
			}
		}

		function addEventListeners() {
			$(window).on(rshm.Input.RESIZE, resize);
			button.addEventListener('click', button_clickHandler, false);
			menu.addEventListener('click', outside_clickHandler, false);
			closeBtn.addEventListener('click', closeBtn_clickHandler, false);
			menuArea.addEventListener('click', outside_clickHandler, false);
		}

		function button_clickHandler(e) {
			e.preventDefault();
			isOpen = !isOpen;
			toggle(isOpen);

		}

		function closeBtn_clickHandler(e) {
			e.preventDefault();
			toggle(false);
			isOpen = false;
		}

		function outside_clickHandler(e) {
			e.stopPropagation();
			toggle(false);
			isOpen = false;
		}

		function toggle(show) {
			if (!show) {
				hideMenu();
			} else {
				showMenu();
			}
		}

		function hideMenu() {
			isOpen = false;

			if (Prefixr.transition) {
				body.style[Modernizr.prefixed('transform')] = 'translate3d(0, 0, 0)';
				element.style[Modernizr.prefixed('transform')] = 'translate3d(0, 0, 0)';
				menuArea.style[Modernizr.prefixed('transform')] = 'translate3d(0, 0, 0)';
			}

			setTimeout(function() {
				$('body').removeClass(SETTINGS.open);
			}, 750);

			body.removeEventListener(rshm.Input.START, outside_clickHandler, false);

			// enable scrolling
			menuArea.removeEventListener('touchmove', body_scrollHandler, false);
		}

		function showMenu() {
			isOpen = true;

			if (Prefixr.transition) {
				body.style[Modernizr.prefixed('transform')] = 'translate3d(255px, 0, 0)';
				element.style[Modernizr.prefixed('transform')] = 'translate3d(255px, 0, 0)';
				menuArea.style[Modernizr.prefixed('transform')] = 'translate3d(255px, 0, 0)';
			}

			menuArea.style.opacity = 1;

			$('body').addClass(SETTINGS.open);

			body.addEventListener(rshm.Input.START, outside_clickHandler, false);

			// disable scrolling
			menuArea.addEventListener('touchmove', body_scrollHandler, false);
		}

		function body_scrollHandler(e) {
			e.preventDefault();
			e.stopPropagation();
		}

		// public methods and properties
		return {
		};
	};

	$.fn.hamburger = function(options) {
		return this.each(function() {
			return new rshm.Hamburger(this, options);
		});
	};

}(window.rshm = window.rshm || {}, jQuery || $));