(function($){
	'use strict';



	window.ScrollSnapper = function(){
		var $window = $(window),
			windowHeight = 0,
			scrollPos = 0,
			direction = 1,
			snapPoints = [],
			settings = {},
			timeout = false,
			currentSnapPoint = false,
			animating = false,
			disabled = false,
			updateTimeout = false,
			$htmlAndBody = $('html,body');

		function config(options){
			settings = $.extend({
				duration: 400,
				easing: 'swing',
				snapTo: false,
				resizeTargets: false,
				beforeSnap: false,
				afterSnap: false,
				radius: 100
			}, options);
			updateSnapPoints();
		}

		function updateSnapPoints(){
			if (settings.snapTo){
				snapPoints = [];
				$.each(settings.snapTo, function(i, el){
					snapPoints.push( $(el).offset().top );
				});
			}
		}

		function updateHeight(){
			// give the DOM time to process reflows
			clearTimeout(updateTimeout);
			updateTimeout = setTimeout(function(){
				updateSnapPoints();
				snap();
			},20);
		}

		function snap(){
			var top = $window.scrollTop() + ($window.height() * 0.45 * direction),
				next = 0,
				prev = 0,
				prevDelta = 9999999,
				nextDelta = 9999999,
				snapTop = false,
				h = $window.height();

			$.each(snapPoints, function(i, point){
				if (top < point){
					next = i;
					return false;
				}else{
					prev = i;
				}
			});


			prevDelta = Math.abs(top - snapPoints[prev]);
			nextDelta = Math.abs(top - snapPoints[next]);

			if (prevDelta <= nextDelta){
				currentSnapPoint = prev;
			}else{
				currentSnapPoint = next;
			}

			animateTopSnapPoint(currentSnapPoint);
		}

		function animateTopSnapPoint(snapPoint, duration){

			animating = true;


			var started = false,
				ended = false;

			$htmlAndBody.stop().animate({
				scrollTop: snapPoints[snapPoint]
			}, {
				duration: duration || settings.duration,
				easing: settings.easing,
				start: function(){
					if (started){ return; }
					
					if (typeof settings.beforeSnap === 'function'){
						settings.beforeSnap.apply(window, [snapPoint, snapPoints.length - 1]);
					}

					started = true;
				},
				complete: function(){
					if (ended){ return; }

					if (typeof settings.afterSnap === 'function'){
						settings.afterSnap.apply(window, [snapPoint, snapPoints.length - 1]);
					}
					setTimeout(function(){
						animating = false;
					},50);

					ended = true;
				}
			});
		}

		function stop(){
			if (animating){
				$htmlAndBody.stop();
			}
		}

		function prev(){
			if (currentSnapPoint >= 1){
				currentSnapPoint--;
			}
			animateTopSnapPoint(currentSnapPoint);
		}

		function next(){
			if (currentSnapPoint < snapPoints.length - 1){
				currentSnapPoint++;
			}
			animateTopSnapPoint(currentSnapPoint);
		}

		function goTo(snapPoint, duration){
			currentSnapPoint = snapPoint;

			if (currentSnapPoint >= snapPoints.length - 1){
				currentSnapPoint = snapPoints.length - 1;
			}else if (currentSnapPoint < 0){
				currentSnapPoint = 0;
			}
	
			animateTopSnapPoint(currentSnapPoint, duration);
		}

		function onScroll(e){
			if (!animating && !disabled){
				stop();
				var p = $window.scrollTop();
				if (p < scrollPos){
					direction = -1;
				}else{
					direction = 1;
				}
				scrollPos = p;

				clearTimeout(timeout);
				timeout = setTimeout(function(){
					updateHeight();
				}, 200);
			}
		}

		function onResize(){
			if (!disabled){
				stop();
				updateHeight();
			}
		}

		function touchStart(e){
			$htmlAndBody.stop();
		}

		function touchMove(e){
			if (!disabled){
				$(window).trigger('scroll');
			}
		}

		function keyDown(e){
			if (e.keyCode === 38) {
				prev();
				return false;
			}else if(e.keyCode === 40){
				next();
				return false;
			}
		}

		function enable(){
			updateHeight();
			disabled = false;
		}

		function disable(){
			disabled = true;
		}

		(function init(){
			if ('ontouchstart' in window){
				//$(document).on('touchstart', touchStart);
				$(document).on('touchmove', touchMove);
			}
				
			$window.scroll(onScroll);

			config({});
			updateHeight();
			$window.resize(onResize);
			$(document).keydown(keyDown);


			setTimeout(function(){
				updateHeight();
			}, 1000);

		})();

		return {
			config: config,
			next: next,
			prev: prev,
			goTo: goTo,
			enable: enable,
			disable: disable
		};
	};

	
})(jQuery);



