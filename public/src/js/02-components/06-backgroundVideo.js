;(function ($) {

	window.BackgroundVideo = function(_options){
		// defaults
		var defaults = {
			ratio: 16/9,
			container: document.body
		},
		options = $.extend({}, defaults, _options),
		width = 0,
		height = 0,
		video,
		source,
		source2;

		function createDOM(){
			video = document.createElement('video');
			video.className = 'background-video';
			video.id = 'background-video';
			video.style.width = '100%';
			video.style.height = '100%';
			video.loop = true;

			source = document.createElement('source');
			source.className = 'background-video-mp4';
			source.src = 'http://cdn.rollingstone.com/feature/cult-classics/media/cult-classics-2.mp4';
			source.type = 'video/mp4';

			source2 = document.createElement('source');
			source2.className = 'background-video-ogg';
			source2.src = 'http://cdn.rollingstone.com/feature/cult-classics/media/cult-classics-2.ogv';
			source2.type = 'video/ogg';

			video.appendChild(source);
			video.appendChild(source2);

			$(options.container).prepend(video);

			resize();

			video.play();
		}

		$(window).resize(resize);

		function resize(){
			var playerWidth,
				playerHeight;

			width = options.container.width();
			height = options.container.height();

			 if (width / options.ratio < height) {
				playerWidth = Math.ceil(height * options.ratio);
				$('.background-video').width(playerWidth).height(height).css({left: (width - playerWidth) / 2, top: 0});
			} else {
				playerHeight = Math.ceil(width / options.ratio);
				$('.background-video').width(width).height(playerHeight).css({left: 0, top: (height - playerHeight) / 2});
			}
		}

		(function init(){
			options.container = $(options.container);
			createDOM();
		})();
	};

})(jQuery);