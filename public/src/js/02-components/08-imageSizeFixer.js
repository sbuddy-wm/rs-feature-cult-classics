;(function imageSizeFixer(){
	'user strict';

	var $images = $('.right-column .editorial-image'),
		timeout = false;

	function update(image){

		$.each(image || $images, function(i, el){
			var $el = $(el),
				$image = $el.find('img'),
				elWidth = $el.width(),
				elHeight = $el.height(),
				imgWidth = $image.data('width') || $image.width(),
				imgHeight = $image.data('height') || $image.height(),
				top = 0,
				left = 0;

			if (!$image.data('width') && imgWidth > 0){
				$image.data('width', imgWidth);
				$image.data('height', imgHeight);
			}

			if (elWidth > elHeight){
				width = elWidth;
				height = (imgHeight/imgWidth) * elWidth;
				top = -(height - elHeight)/2;
			}else{
				height = elHeight;
				width = (imgWidth/imgHeight) * elHeight;
				left = -(width - elWidth)/2;
			}

			if (width == 0) {
				width = $image.prop('naturalWidth');
				height = $image.prop('naturalHeight');
			}

			$image.css({
				'height': height,
				'width': width,
				'margin-top': top,
				'margin-left': left
			});
		});
	}

	$(window).resize(function(){
		clearTimeout(timeout);
		timeout = setTimeout(update, 500);
	});

	$('.right-column .editorial-image img').load(function(){
		update($(this).parent('.editorial-image'));
	}).each(function() {
	  	if(this.complete) $(this).load();
	});

	return {
		update: update
	};
}());