/**
 * @SocialBox Compontent for sharing functionality
 * @author Andres Garcia <andres.garcia@zemoga.com>
 */

/*global jQuery */
/*global window */
/*global console */

(function(rshm, $) {
	'use strict';

	rshm.SocialBox = (function() {

		var $DOMContainer = $('.social-box'),
			$socialBoxDialog = $DOMContainer.find('.social-box-dialog'),

			$sharingDialogOptionFacebook = $socialBoxDialog.find('.sharing-dialog-option-facebook a'),
			$sharingDialogOptionTwitter = $socialBoxDialog.find('.sharing-dialog-option-twitter a'),
			$sharingDialogOptionGoogle = $socialBoxDialog.find('.sharing-dialog-option-google a'),
			$sharingDialogOptionPinterest = $socialBoxDialog.find('.sharing-dialog-option-pinterest a'),

			descriptionString = $('meta[name="description"]').attr('content'),
			descriptionStringEncoded = window.escape(descriptionString),
			imagePath = $('meta[property="og:image"]').attr('content'),
			imagePathEncoded = window.escape(imagePath),
			documentTitle = window.document.title,
			documentTitleEncoded = window.escape(documentTitle),
			documentURL = window.location.origin + window.location.pathname,
			documentURLEncoded = window.escape(documentURL),
			documentShortURL = $DOMContainer.data('shortUrl'),
			documentShortURLEncoded = window.escape(documentShortURL),

			Items = {
				TWITTER_URL: function(data) {

				}
			},

			TWITTER_URL = 'https://twitter.com/intent/tweet?url=' + documentShortURLEncoded + '&text=',
			FACEBOOK_URL = 'https://www.facebook.com/sharer/sharer.php?s=100&p[url]=',
			GOOGLE_URL = 'https://plus.google.com/share?url=',
			PINTEREST_URL = '//www.pinterest.com/pin/create/bookmarklet/?url=' + documentShortURLEncoded + '&is_video=false&media=' + imagePathEncoded + '&description=' + descriptionStringEncoded;

	/*@newWindow('https://www.facebook.com/sharer/sharer.php?s=100&
	p[title]=' + encodeURIComponent(@productInfo.title) + 
	'&p[summary]=' + encodeURIComponent(@productInfo.summary) + 
	'&p[url]=' + encodeURIComponent(@productInfo.url  + '&mv=facebook') + 
	'&p[images][0]=' + encodeURIComponent(@productInfo.thumbnailUrl))*/

		function setupSocial(){

			$sharingDialogOptionTwitter.attr('href', TWITTER_URL + window.escape($sharingDialogOptionTwitter.data('sharingText')));
			$sharingDialogOptionFacebook.attr('href', FACEBOOK_URL);
			$sharingDialogOptionGoogle.attr('href', GOOGLE_URL);
			$sharingDialogOptionPinterest.attr('href', PINTEREST_URL);
		}

		function update(hash, name, caption, description, picture) {
			$sharingDialogOptionTwitter.attr('href', 'https://twitter.com/intent/tweet?url=' + encodeURIComponent(hash) + '&text=' + encodeURIComponent(name));
			$sharingDialogOptionFacebook.attr('href', FACEBOOK_URL + window.escape(hash));
			$sharingDialogOptionGoogle.attr('href', GOOGLE_URL + window.escape(hash));
			$sharingDialogOptionPinterest.attr('href', PINTEREST_URL + ' ' + window.escape(hash));

			$sharingDialogOptionFacebook.unbind().bind('click', function(e){
				e.preventDefault();

				var data = {
			        method: 'feed',
			        name: name || documentTitle,
			        link: window.location.origin + window.location.pathname + hash,
			        description: description || ''
			    };
			    
			    if (caption){
			    	data.caption = caption;
			    }

			    data.picture = 'http://cdn.rollingstone.com/feature/cult-classics/img/shareable/og.jpg';

				FB.ui(data, function(response) {
			        if(response && response.post_id){}
			        else{}
			    });
			});
		}

		/**
		 * @constructor
		 */
		(function init() {
			setupSocial();
		}());

		return {
			update: update
		};
	}());
}(window.rshm = window.rshm || {}, jQuery));