/**
 * @fileOverview Dialog component
 * @author Juan David Andrade <juan.andrade@zemoga.com>
 * @version 1.0.0
 */

/*global jQuery:true, $:true, rshm:true, Prefixr:true */
(function(rshm, $) {
  'use strict';

  /**
   * Represents a Dialog component
   * @constructor
   * @return {Object} Exposed methods
   */
  rshm.Dialog = function(selector, options) {

    /**
     * Main container
     * @type {HTMLElement}
     */
    var element,
      /**
       * Overlay
       * @type {HTMLElement}
       */
      backdrop,
      /**
       * Close button
       * @type {HTMLElement}
       */
      closeButton,
      /**
       * Indicates that the dialog element is active and that the user can interact with it
       * @type {Boolean}
       */
      open = false,
      /**
       * Returns the dialog's return value.
       * @type {String}
       */
      returnValue = '',
      /**
       * Checks if the browser supports natively this feature
       * if don't, a fallback will be applied to support them
       * @type {Boolean}
       */
      dialogSupported = false,
      /**
       * Default settings
       * @type {Object}
       */
      SETTINGS = {
        backdrop: 'dialog-backdrop',
        closeButton: '.dialog-close',
        skin: 'dialog-skin',
        shown: undefined,
        closed: undefined
      };

    /**
     * @constructs Input
     */
    (function() {
      $.extend(SETTINGS, options);

      init();
    }());

    /**
     * Initialize component
     */
    function init() {
      element = document.querySelector(selector);
      closeButton = element.querySelector(SETTINGS.closeButton);

      //dialogSupported = supportsDialog();

      //if (!dialogSupported) {
        $(element).addClass(SETTINGS.skin);
      //}
      addEventListeners();
    }

    /**
     * Add event handlers
     * @private
     */
    function addEventListeners() {
      closeButton.addEventListener(rshm.Input.CLICK, closeButton_clickHandler);
      //window.addEventListener(rshm.Input.RESIZE, window_resizeHandler);
    }

    function supportsDialog() {
      return false;//(typeof document.createElement('dialog').showModal === 'function');
    }

    /**
     *
     * @private
     */
    function createBackdrop() {
      element.insertAdjacentHTML('afterend', '<div class="backdrop ' + SETTINGS.backdrop + '"></div>');
      backdrop = document.querySelector('body > .' + SETTINGS.backdrop);
      backdrop.addEventListener('click', closeButton_clickHandler);

      function disable(e){
        e.preventDefault();
        e.stopPropagation();
      }
      backdrop.addEventListener('mousewheel', disable);
      element.addEventListener('mousewheel', disable);
      backdrop.addEventListener('DOMMouseScroll', disable);
      element.addEventListener('DOMMouseScroll', disable);
      backdrop.addEventListener('touchmove', disable);
      element.addEventListener('touchmove', disable);

      //backdrop.className += ' open';
    }

    /**
     * 
     * @private
     */
    function removeBackdrop() {
      if (backdrop){
        backdrop.removeEventListener(rshm.Input.CLICK, closeButton_clickHandler);
        backdrop.parentNode.removeChild(backdrop);
      }
    }

    /**
     * Displays the dialog element
     * @return {[type]} [description]
     */
    function show() {
      /*if (dialogSupported) {
        element.showModal();
        return;
      }*/

      // If the element already has an open attribute, then abort these steps
      if (open) {
        return;
      }

      // Add an open attribute to the dialog element, whose value is the empty string
      open = true;

      // center element
      var vCenter = -element.offsetHeight * 0.5;
      element.style.marginTop = vCenter + 'px';

      $(element).addClass('open');
      $(backdrop).addClass('open');
      //element.setAttribute('open', open);

      if (typeof SETTINGS.shown === 'function') {
        SETTINGS.shown();
      }
    }

    /**
     * Displays the dialog element and makes it the top-most modal dialog
     * @return {[type]} [description]
     */
    function showModal(shown) {
      // If the element already has an open attribute, then abort these steps
      if (open) {
        return;
      }

      $(element).css('opacity', 1);

      if (typeof shown !== 'undefined') {
        SETTINGS.shown = shown;
      }

      //if (!dialogSupported) {
        createBackdrop();
      //}

      show();
    }

    /**
     * Closes the dialog element
     * @return {[type]} [description]
     */
    function close() {
      if (typeof SETTINGS.closed === 'function') {
        SETTINGS.closed();
      }

      $(element).css('opacity', 0);
      setTimeout(function(){
        $(element).removeClass('open');
        removeBackdrop();
      }, 500);

      // If subject does not have an open attribute, then abort these steps.
      if (!open) {
        return;
      }

      open = false;
    }

    /**
     * Close button clicked
     * @event
     */
    function closeButton_clickHandler(e) {
      e.preventDefault();
      close();
    }

    /**
     * window resized / device orientation changed
     * @event
     */
    function window_resizeHandler(e) {
      //console.log("resized");
    }

    // public methods
    return {
      show: show,
      showModal: showModal,
      close: close
    };
  };

  /**
   * Create jQuery plugin
   * @param  {Object} options Overwritten settings
   * @return {Dropdown}
   */
  $.fn.dialog = function(options) {
    return this.each(function() {
      return new rshm.Dialog(this, options);
    });
  };
 
}(window.rshm || {}, jQuery || $));