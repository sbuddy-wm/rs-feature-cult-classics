/*!
 * scrollPointTrigger.js
 */
/*global jQuery */


(function($) {
'user strict';

	var scrollPointTriggers = [],
		timeout,
		scrollTimeout,
		_scrollTop = 0,
		$window = $(window);



	function ScrollPointTrigger(scrollPoint, callback, settings) {
		var offsetTop,
			scrollPointTop,
			currentScrollTop = 0,
			used = false;

		function execute(direction){
			if (settings.once && used){
				return;
			}
			used = true;
			callback.apply(scrollPoint, [direction]);
		}

		function onScroll(e){
			var scrollTop = $(this).scrollTop(),
				top = scrollPointTop;

			if (!e && scrollTop == top){
				// handle page load position
				execute(null);
			}

			if (currentScrollTop <= top && scrollTop > top){
				// passed scrolling down
				if (settings.direction === 'down' || settings.direction === 'both'){
					execute('down');
				}
			}

			if (currentScrollTop >= top && scrollTop < top){
				// passed scrolling up
				if (settings.direction === 'up' || settings.direction === 'both'){
					execute('up');
				}
			}

			currentScrollTop = scrollTop;
		}

		function onResize(e){
			offsetTop = $(scrollPoint).offset().top;
			scrollPointTop = offsetTop + (typeof settings.offset === 'function' ? settings.offset.apply(scrollPoint, [offsetTop]) : settings.offset);
			if (scrollPointTop < 0){
				scrollPointTop = 0;
			}
		}

		function listen(){
			onResize();
			onScroll();
			setTimeout(function(){
				onResize();
			}, 1000);
		}

		listen();

		return {
			onScroll: onScroll,
			onResize: onResize
		};
	}


	// Centralize everything in just one scroll listener for performance
	
	function onScroll(e){
		for (var i in scrollPointTriggers){
			if (typeof scrollPointTriggers[i] === 'object'){
				scrollPointTriggers[i].onScroll.apply(this, [e]);
			}
		}
	}
	$window.scroll(onScroll);


	function resizeAll(){
		for (var i in scrollPointTriggers){
			if (typeof scrollPointTriggers[i] === 'object'){
				scrollPointTriggers[i].onResize.apply(this, []);
			}
		}
	}

	$(window).resize(function(e){
		resizeAll();
		clearTimeout(timeout);
		timeout = setTimeout(resizeAll, 80);
	});


	$.fn.onScrollPoint = function(callback, options) {

		var settings = $.extend({
			direction: 'both', // 'up', 'down'
			once: false,
			offset: 0
		}, options);


		return this.each(function() {
			scrollPointTriggers.push(new ScrollPointTrigger(this, callback, settings));
		});

	};
}(jQuery));