/**
 * @fileOverview Project initialization
 * @version 1.0.0
 */

/* global $,Modernizr */

$(function(){

	var videoIframe = document.querySelector('#video-dialog iframe'),
		videoDialog = new rshm.Dialog('#video-dialog', {
			closed: function() {
				if (editorialVideoPlayer){
					if (editorialVideoPlayer.tagName) {
						editorialVideoPlayer.src = "";
					} else {
						editorialVideoPlayer.stopVideo();
					}
				}
			}
		}),
		editorialVideoPlayer,
		youtubePlayer = false,
		indexDialog = new rshm.Dialog('#site-index-dialog'),
		introVideoPlayer,
		menu = new rshm.Hamburger('.header'),
		$window = $(window),
		$pager = $('.pager'),
		$scrollDownBtn = $('.scroll-btn'),
		$backBtns = $('.js-back-to-top'),
		$pages,
		$sections = $('.main-content section'),
		$next = $('.next-btn'),
		totalSections = $sections.length,
		changeUIColors = false,
		snapper = new ScrollSnapper(),
		sectionTimeout,
		initialSection = (rshm.utils.getQueryVariable('section') || '').replace(/^\//ig, ''),
		imageResizer,
		is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1,
		is_ie = navigator.userAgent.toLowerCase().indexOf('msie ') > -1 ||
				navigator.userAgent.toLowerCase().indexOf('trident') > -1;

	// include youtube js api
	var scriptTag = document.createElement('script');
    scriptTag.src = "//www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(scriptTag, firstScriptTag);

	$('.editorial-video').on('click', function(e) {
		e.preventDefault();

		dataLayer.push({
			'event': 'gaEvent',
			'gaEventCategory': $(this).data('category'),
			'gaEventAction': $(this).data('action'),
			'gaEventLabel': $(this).data('label')
		});

		var link = $(e.currentTarget).attr('href').replace('#', '');

		videoDialog.showModal(function() {

			$('#youtube-player').show();

			if (link.length > 11) {
				editorialVideoPlayer = videoIframe;
				editorialVideoPlayer.src = 'http://cache.vevo.com/m/html/embed.html?video=' + link;
			} else {
				if (!youtubePlayer) {
					youtubePlayer = new YT.Player('youtube-player', {
						playerVars: {
							html5: 1
							},
							events: {
							'onReady': editorialVideoPlayer_playerReady,
							'onStateChange': editorialVideoPlayer_playerStateChange
						}
					});
				}
				editorialVideoPlayer = youtubePlayer;

				videoIframe.src = 'http://www.youtube.com/embed/' + link + '?autoplay=1&rel=0&enablejsapi=1';
			}

			console.log(editorialVideoPlayer);

			// update video URL
			function editorialVideoPlayer_playerStateChange(e) {
				if (e.data === 0) {
					videoDialog.close();
				}
			}

			function editorialVideoPlayer_playerReady(e) {
				if (editorialVideoPlayer) {
					editorialVideoPlayer.unMute();
				}
			}

		});
	});

	document.querySelector('.open-index').addEventListener(rshm.Input.CLICK, function(e) {
		e.preventDefault();
		indexDialog.showModal();
	});

	// link clicked from index nav
	$('.index-nav a').on('click', function(e) {
		e.preventDefault();
		indexDialog.close();
		var index = parseInt($(e.currentTarget)[0].className.replace(/[a-z- ]/ig, ''), 10);
		var currentSlide = $('.main-content').find('.slide-'+index).index();
		console.log(index);
		snapper.goTo(index);
	});

	// Share > Mobile
	$('.share-btn').on('click', function(e) {
		e.preventDefault();
		$('.social-bar').addClass('share-expanded');
	});

	$('.share-close-btn').on('click', function(e) {
		e.preventDefault();
		$('.social-bar').removeClass('share-expanded');
	});

	snapper.config({
		snapTo: $('section'),
		beforeSnap: function(pos, total){
			var $section = $($sections.get(pos)),
				bgColor = $section.css('background-color'),
				$editorial = $section.find('.editorial-body'),
				position = $editorial.position();

			if (changeUIColors && position){
				// resize copy section height to prevent going behind the NEXT btn
				//$editorial.css('height', $window.height() - position.top - 110);
			}
		},
		afterSnap: function(pos, total){
			var $section = $($sections.get(pos)),
				bgColor = $section.css('background-color'),
				$editorial = $section.find('.editorial-body'),
				position = $editorial.position();
				
				
			var dataName = $section[0].dataset.name;

			$('.index-nav a.active').css({'background-color': '', 'color': ''}).removeClass('active');
			$('.index-nav a.index-' + pos).addClass('active').css({'background-color': bgColor, 'color': '#000' });
			
			if (dataName){
				
				$('.sprite-logo, .icon-twitter, .icon-facebook, .icon-google-plus, .icon-pinterest, .next-btn').data('action',dataName);
				
			}
			
			updateSectionType($section);
/*
			if (position){
				if (changeUIColors){
					// resize copy section height to prevent going behind the NEXT btn
					$editorial.css('height', $window.height() - position.top - 110);
				}
			}*/
			$('.nano').nanoScroller();
		}
	});
	
	$('.sprite-logo, .icon-twitter, .icon-google-plus, .icon-pinterest, .next-btn, .open-index, .media-object, .back-btn').click(function(){
		dataLayer.push({
			'event': 'gaEvent',
			'gaEventCategory': $(this).data('category'),
			'gaEventAction': $(this).data('action'),
			'gaEventLabel': $(this).data('label')
		});		
	});	
	
	
	$('.sharing-dialog-option-facebook').click(function(){
		console.log('Facebook clicked');
		dataLayer.push({
			'event': 'gaEvent',
			'gaEventCategory': $('.icon-facebook').data('category'),
			'gaEventAction': $('.icon-facebook').data('action'),
			'gaEventLabel': $('.icon-facebook').data('label')
		});					
	});
	

	// shared section
	if (initialSection) {
		initialSection = $('a[name='+initialSection+']').parent('section').index();
		setTimeout(function() {
			snapper.goTo(parseInt(initialSection, 10));
		}, 1500);
	}

	// create pager
	$.each($sections, function(i, el){
		(function(element){
			var itemClassName = ($(element).hasClass('section-editorial')) ? '' : 'pager-hidden';
			var item = $('<li class="' + itemClassName + '"></li>').on(rshm.Input.CLICK, function(e){
				e.preventDefault();
				var sectionIndex = $(element).index(),
					delta = Math.abs(rshm.currentSection - sectionIndex);

				snapper.goTo( sectionIndex, 100 * delta );
			});
			$pager.append(item);
		}(el));
	});
	$pages = $pager.find('li');

	$next.on(rshm.Input.CLICK, function(e){
		e.preventDefault();
		history.pushState(null, null, '#' + rshm.nextSectionId);
		snapper.next();
	});

	$scrollDownBtn.on(rshm.Input.CLICK, function(e) {
		e.preventDefault();
		history.pushState(null, null, '#' + rshm.nextSectionId);
		snapper.goTo(1);
	});

	$backBtns.on(rshm.Input.CLICK, function(e) {
		e.preventDefault();
		snapper.goTo(0, rshm.currentSection * 100);
	});

	function updateSectionType($section){
		// update body class
		var newSectionType = ($section[0].className.match(/section-[^ ]+/ig) || ' ')[0];
		if (!(new RegExp(newSectionType)).test(document.body.className)){
			document.body.className = document.body.className.replace(/section-[^ ]+/ig, '');
			document.body.className += ' '+newSectionType;
		}
	}

	$sections.onScrollPoint(function(direction){
		
		var $section;

		// get current section color and set it on header
		var currentBackgroundColor;
		if (direction == 'up'){
			$section = $(this).prev('section');
			history.pushState(null, null, '#' + $section.children('a').attr('name'));
		}else{
			$section = $(this);
			if ($section.index() > 0) {
				history.pushState(null, null, '#' + rshm.nextSectionId);
			}
		}

		if (!$section.length){
			return;
		}

		updateSectionType($section);

		$section.addClass('viewed');
		
		$('.nano').nanoScroller({ scroll: 'top' });

		currentBackgroundColor = $section.css('background-color');
		// clear pager
		$pages.removeClass('current');
		$pages.eq($section.index()).addClass('current');
		rshm.currentSection = $section.index();
		rshm.nextSectionId = $section.next().children('a:first').attr('name');

		var shareURL = 'http://www.rollingstone.com/culture/features/cult-classics-a-to-z/#' + $section.find('.share-url').attr('href'),
			shareTitle = 'Cult Classics: A to Z',
			shareDescription = "From Arrested Development to Zappa",
			shareCaption = '',
			shareImage = $section.find('.share-url').data('share-image');
		
		if ($section.hasClass('section-editorial')){
			var title = ($section.find('.editorial-heading').text() || '').replace('<br>', ''),
				subTitle = $section.find('.editorial-subheading').text();

			if (title){
				shareTitle = title;
			}
			if (subTitle){
				shareTitle += ': ' + subTitle;
			}
		}else if ($section.hasClass('section-advertising')){
			shareTitle = $section.find('.editorial-heading').text();
			shareCaption = $section.find('.editorial-subheading').text();
		}

		// update share links
		$('.sharing-dialog-option-twitter a').data('sharing-text', shareTitle);
		rshm.SocialBox.update(shareURL, shareTitle, shareCaption, shareDescription, shareImage);
							  // hash, name, caption, description, picture

		clearTimeout(sectionTimeout);
		sectionTimeout = setTimeout(function(){
			// asume that after 2s in a certain section,
			// that section counts as viewed.
			dataLayer.push({
				'event': 'gaVirtualPV',
				'gaVirtualUrl': location.pathname + '#' + $section.find('.share-url').attr('href')
			});

			if (Modernizr.mq('(max-width: 1023)') || $window.width() <= 1023) {
				var newSectionType = ($section[0].className.match(/section-[^ ]+/ig) || ' ')[0];
				document.body.className = document.body.className.replace(/section-[^ ]+/ig, '');
				document.body.className += ' '+newSectionType;
			}

			

			//$section.find('.nano').nanoScroller({ scroll: 'top' });
		}, 2000);

		if (changeUIColors){
			if(/26 *, *26 *, *26/.test(currentBackgroundColor) || $section.hasClass('theme-adv')){
				currentBackgroundColor = 'transparent';
			}
		}
	}, {
		offset: -26 // half the height of the header element
	});


	// NEXT button scroll logic
	(function (){
		var $nextButtonSections,
			timeout,
			currentSectionHeight = $sections.eq(0).height(),
			$nextWrapper  = $next.find('.wrapper');

		$.each($sections, function(i, el){
			var $el = $(el),
				$div = $('<div />');

				if (!$el.hasClass('theme-adv')) {
					$div.css('background',$el.find('.right-column').css('background-color'));
				}

			$nextWrapper.append($div);
		});

		$nextButtonSections = $nextWrapper.find('div');

		function onResize(){
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				currentSectionHeight = $sections.eq(0).height();
				$nextButtonSections.height(currentSectionHeight);
				onScroll();
			}, 4000);
		}

		$window.resize(onResize);
		onResize();

		function onScroll(e){
			$nextWrapper.scrollTop($window.scrollTop() + currentSectionHeight - 58);
		}

		$window.scroll(onScroll);
		onScroll();
	})();

	function skinScrollBars(){
		setTimeout(function(){
			$('.nano').nanoScroller();
			$('.nano').find('.nano-content').css('position', 'absolute');
		}, 800);
	}

	function unskinScrollBars(){
		setTimeout(function(){
			$('.nano').find('.nano-content').css('position', 'static');
			$(".nano").nanoScroller({ destroy: true });
		}, 800);
	}

	// this happens once the page loads
	if (Modernizr.mq('(min-width: 1025px)') || $window.width() > 1025) {
		// enable background video for 1025+
		introVideoPlayer = new BackgroundVideo({
			container: $('.intro-video')
		});
	}

	if (Modernizr.mq('(min-width: 1024px)') || $window.width() > 1024) {
		// desktop only stuff
		changeUIColors = true;
		skinScrollBars();
	}else{
		snapper.disable();
		unskinScrollBars();
	}

	if (Modernizr.mq('(max-width: 768px)') || $window.width() < 769) {
		$('.intro .scroll-text').text('Swipe Down');
	}

	// flag to make if-blocks only execute once
	// instead of every resize event
	var currentBreakpoint,
		$scrollbars = false;

	// this happens during a resize
	$window.resize(function(){
		if (Modernizr.mq('(min-width: 1024px)') || $window.width() > 1024) {
			// 1024 +
			if (currentBreakpoint != 1024){
				changeUIColors = true;
				snapper.enable();
				currentBreakpoint = 1024;
				skinScrollBars();
			}
			$('.intro .scroll-text').text('Scroll Down');

		}else if (Modernizr.mq('(min-width: 768px)') || $window.width() > 768){
			// 768 to 1023
			if (currentBreakpoint != 768){
				changeUIColors = false;
				snapper.disable();
				currentBreakpoint = 768;
				unskinScrollBars();
			}
			$('.intro .scroll-text').text('Scroll Down');

		}else if (Modernizr.mq('(min-width: 320px)') || $window.width() > 320){
			// 320 to 767
			if (currentBreakpoint != 320){
				changeUIColors = false;
				snapper.disable();
				currentBreakpoint = 320;
				unskinScrollBars();
			}
			$('.intro .scroll-text').text('Swipe Down');
		}
	});	

}());
